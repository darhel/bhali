
const state  =
{
    paginator:{},
    items:[],
    article:[],
    articleAsParameter:[],
    loading:false,
    loaded:false,
    currentPage:0
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    article: state => {
      return state.article
  },
};

const actions =
{
  async getArticle({commit},payload)
  {
     commit('SET_LOADING',true)


     var result = await axios.get('/api/articles/'+payload).then( response =>
          {
            commit('SET_ARTICLE',response.data)
            commit('SET_LOADING',false)
            commit('SET_LOADED',true)
          })

      return result;
  },
   async getArticles({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

       var result = await axios.get(payload.page=="" ? '/api/articles/' :'/api/articles/'+payload.query).then( response =>
            {
              commit('SET_ARTICLES',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async updateArticle({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/articles/'+payload.id,payload).then( response =>
            {

              commit('UPDATE_ARTICLE',response.data.article)
              commit('SET_LOADING',false)
            })
        return result;
    },

    async addArticle({commit},payload)
    {

       commit('SET_LOADING',true)

       const result = await axios.post('/api/articles',payload).then( response =>
            {
              console.log(response.data.article)
              commit('ADD_ARTICLE',response.data.article)
              commit('SET_LOADING',false)

            })

        return await result

    },

    async addComment({commit},payload)
    {
       commit('SET_LOADING',true)

       const result = await axios.post('/api/comments',payload).then( response =>
            {
              commit('ADD_COMMENT',response.data.comment)
              commit('SET_LOADING',false)

            })
        return await result
    },
    async removeComment({commit},payload)
    {
        var result = await axios.delete('/api/comments/'+payload).then( response =>
            {
              commit('REMOVE_COMMENT',payload)
              commit('SET_LOADING',false)
            })
      return result;
    },
//

    async removeArticle({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/articles/'+payload.id).then( response =>
            {
              commit('REMOVE_ARTICLE',payload)
              commit('SET_LOADING',false)
            })
      return result;
    },

    init({commit})
    {
            commit('SET_ARTICLES',{})
            // commit('SET_ARTICLE',{})
            commit('SET_ITEMS',[])
            commit('SET_LOADED',false)
            commit('SET_LOADING',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_ARTICLES(state,payload)
    {
        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []

            state.loaded = true
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
           state.paginator.data.forEach(element => {
            state.items.push(element)
          });
          state.loaded = true

        }
    },
    UPDATE_ARTICLE(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_ARTICLE(state,payload)
    {
    //   console.log(payload)
      state.items.unshift(payload)
    },
    ADD_COMMENT(state,payload)
    {
      state.article.comments.push(payload)
    },
    REMOVE_ARTICLE(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },
    SET_ITEMS(state,payload)
    {
        state.items = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    },
    SET_ARTICLE(state,payload)
    {
        state.article = payload
    },
    REMOVE_COMMENT(state,payload)
    {
      const index = state.article.comments.findIndex(elem =>  elem.id===payload)

      if(index!==-1)
        {
          state.article.comments.splice(index,1)
        }
    },

}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
