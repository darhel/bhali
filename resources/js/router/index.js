import Vue from 'vue'
import Router from 'vue-router'



Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: require('./../pages/Dashboard.vue').default
    },
    {
      path: '/home',
      name: 'home',
      component: require('./../pages/Dashboard.vue').default
    },
    {
      path: '/dashboard',
      name: 'dashboard-two',
      component: require('./../pages/Dashboard.vue').default

     // component: require('./../pages/Dashboard.vue').default
    },
    {
        path: '/blogs',
        name: 'blogs',
        component: require('./../pages/blog/Blogs.vue').default
    },
    {
      path: '/utilisateurs',
      name: 'utilisateurs',
      component: require('./../pages/parametres/Utilisateurs.vue').default
    },
    {
      path: '/trombinoscope',
      name: 'membres',
      component: require('./../pages/membres/Membres.vue').default

    },
    {
      path: '/entreprises',
      name: 'entreprises',
      component: require('./../pages/entreprises/Entreprise.vue').default

    },
    {
      path: '/voitures/ajout',
      name: 'voitures_create',
      component: require('./../pages/voitures/NouvelleVoiture.vue').default

    },
    {
      path: '/entreprises/:id',
      name: 'entreprise_detail',
      component: require('./../pages/entreprises/Detail.vue').default

    },
    {
      path: '/membres/:id',
      name: 'membre_detail',
      component: require('./../pages/membres/Detail.vue').default

    },
    {
      path: '/projets',
      name: 'projets',
      component: require('./../pages/projets/Projets.vue').default

    },
    {
      path: '/locations/ajout',
      name: 'locations_ajout',
      component: require('./../pages/projets/Add.vue').default

    },
    // {
    //   path: '/locations/:id/modification',
    //   name: 'locations_ajout',
    //   component: require('./../pages/projets/Add.vue').default

    // },
    {
      path: '/locations/:id',
      name: 'ShowLocation',
      component: require('./../pages/projets/Show.vue').default

    },
    {
      path: '/marques',
      name: 'marques',
      component: require('./../pages/parametres/Marques.vue').default

    },
    {
      path: '/options',
      name: 'options',
      component: require('./../pages/parametres/Options.vue').default

    },
    {
      path: '/profil',
      name: 'profil',
      component: require('./../pages/profils/Profil.vue').default

    },
    {
      path: '/roles',
      name: 'roles',
      component: require('./../pages/parametres/roles/Roles.vue').default

    },
    // {
    //   path: '/roles/ajout',
    //   name: 'roles',
    //   component: require('./../pages/parametres/roles/Show.vue').default

    // },
    {
      path: '/developper',
      name: 'developper',
      component: require('./../pages/parametres/developper.vue').default

    },
    {
      path: '/roles/:id',
      name: 'roles_details',
      component: require('./../pages/parametres/roles/Show.vue').default

    },
    {
      path: '/depenses',
      name: 'depenses',
      component: require('./../pages/depenses/Depenses.vue').default

    },
    {
      path: '/flux-finances',
      name: 'flux-finances',
      component: require('./../pages/finances/Fluxfinances.vue').default

    },
    {
      path: '/historiques',
      name: 'historiques',
      component: require('./../pages/parametres/historique/Historiques.vue').default

    },
    { path: '/404', name: '404', component: require('./../pages/NotFound.vue').default },
    { path: '*', redirect: '/404' },
    {
      path: '/projets/:id',
      name: 'projet_detail',
      component: require('./../pages/projets/Detail.vue').default
    },
    {
        path: '/register',
        name: 'register',
        component: require('./../auth/Register.vue').default
    },
    {
        path: '/conversation',
        name: 'conversation',
        component: require('./../pages/conversations/Conversation.vue').default
    },
    {
        path: '/email/verify',
        name: 'verify',
        component: require('./../auth/Verify.vue').default
    },
  ],
  linkActiveClass: 'active'
})
