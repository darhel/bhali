<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Bhali | Connexion </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body class="hold-transition login-page">
   <div id="auth">
       @yield('content')

       <flash type="{{ session('type') }}" message="{{ session('status') }}"></flash>
   </div>
   <script  type="application/javascript">
         window.App = {!!
          json_encode(array( 'csrfToken' => csrf_token()));
          !!}

  </script>
   <script src="{{ asset('js/auth.js') }}" defer></script>

</body>

</html>
