@extends('layouts.app')

@section('content')
<verify  inline-template v-cloak>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Vérifiez votre adresse e-mail') }}</div>
                <div class="login-logo">
                    <a href="javascript:void(0)">
                      <img src="{{asset('img/bhali.png') }}" alt="" class="mt-5 mb-2">
                    </a>
                </div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un nouveau lien de vérification a été envoyé à votre adresse e-mail.') }}
                        </div>
                    @endif

                    Bonjour {{ Auth::user()->name }} ! Avant de continuer, veuillez vérifier votre e-mail pour un lien de vérification.
                    {{ __('Si vous n\'avez pas reçu l\'e-mail') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('cliquez ici pour en demander un autre') }}</button>.
                    </form>

                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            <span style="color: red">Déconnectez-vous</span>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</verify>
@endsection
