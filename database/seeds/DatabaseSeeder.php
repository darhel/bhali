<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $countries = Config::get('pays');
        foreach($countries as $country)
        {
            $a = ["code1"=>$country[1],"code2"=>$country[2],"libelle"=>$country[3]];
            factory('App\Pays')->create($a);
        }

        $secteurs = Config::get('secteurs');
        foreach($secteurs as $secteur)
        {
            factory('App\SecteurActivite')->create(["libelle"=>$secteur]);
        }

        $this->call(CompetenceSeeder::class);
        $this->call(FormationSeeder::class);
        // $this->call(RoleSeeder::class);
        // $this->call(PermissionSeeder::class);
    }
}
