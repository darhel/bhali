<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichiers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fichiers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('link')->nullable();
            $table->enum('type',["projet","membre","entreprise"])->default('membre');
            $table->unsignedBigInteger('projet_id')->nullable();
            $table->unsignedBigInteger('membre_id')->nullable();
            $table->unsignedBigInteger('entreprise_id')->nullable();
            $table->timestamps();

            $table->foreign('projet_id')->references('id')->on('projets')->onDelete('set null');
            $table->foreign('membre_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('entreprise_id')->references('id')->on('entreprises')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fichiers');
    }
}
