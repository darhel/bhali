<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Contact::class, function (Faker $faker) {
    return [
        'phone1' => $faker->phoneNumber,
        'phone2' => $faker->phoneNumber,
        'email' => $faker->safeEmail,
        'siteweb' => $faker->ur,
        'facebook' => $faker->word,
        'linkedin' => $faker->word,
        'twitter' => $faker->word,
    ];
});
