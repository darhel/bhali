<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\Formation;
use App\SecteurActivite;
use App\Pays;


$factory->define(App\User::class, function (Faker $faker) {

    $formation = Formation::inRandomOrder()->first();
    $pays = Formation::inRandomOrder()->first();
    $secteur = SecteurActivite::inRandomOrder()->first();

    return [
        'nom' =>$faker->lastName ,
        'prenom' =>$faker->firstName ,
        // 'promotion' => $faker->numberBetween($min = 1950, $max = 2018).""  ,
        'avatar' =>null,
        'civilite' =>  $faker->randomElement(['m.', 'mme', 'mlle', 'pr','dr']),
        'statut_marital' =>  $faker->randomElement(["célibataire","marié(e)","veuf(ve)","concubinage"]),
        'description' => $faker->text($maxNbChars = 150),
        'date_naissance' => $faker->date($format = 'Y-m-d', $max = '2000-01-01'), // '1979-06-09'
        'phone1' => $faker->phoneNumber,
        'phone2' => $faker->phoneNumber,
        'email' => $faker->safeEmail,
        'facebook' => "https://www.facebook.com/".$faker->word,
        'twitter' => "https://www.twitter.com/".$faker->word,
        'linkedin' => "https://www.linkedin.com/".$faker->word,
        'activite_actuelle' => $faker->word,
        'password' => bcrypt("password"),
        'derniere_profession' => $faker->jobTitle,
        'secteur_activite_id' => $secteur ? $secteur ->id : factory(App\SecteurActivite::class),
        'formation_id' => $formation? $formation->id : factory(App\Formation::class),
        // 'pays_residence_id' => $pays ? $pays->id : factory(App\Pays::class),
        'created_by' => factory(App\User::class),
    ];
});
