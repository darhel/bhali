<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Formation;
use App\SecteurActivite;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {

    $formation = Formation::inRandomOrder()->first();
    $pays = Formation::inRandomOrder()->first();
    $secteur = SecteurActivite::inRandomOrder()->first();
    $type = $faker->randomElement(['membre', 'admin']);

    return [
        'name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'description' => $faker->text,
        'avatar' => $faker->word,
        // 'role_id' => factory(App\Role::class),
        'type'=>'membre',
        'password' => bcrypt("password"),
        // 'remember_token' => Str::random(10),
        // 'last_sign_out_at' => $faker->dateTime(),
        // 'last_sign_in_at' => $faker->dateTime(),
        'nom' =>$faker->lastName ,
        'prenom' =>$faker->firstName ,
        // 'promotion' => $faker->numberBetween($min = 1950, $max = 2018),
        'civilite' =>$faker->randomElement(['m.', 'mme', 'mlle', 'pr','dr']),
        'statut_marital' =>$faker->randomElement(["célibataire","marié(e)","veuf(ve)","concubinage"]),
        'date_naissance' =>$faker->date($format = 'Y-m-d', $max = '2000-01-01'), // '1979-06-09'
        'phone1' => $faker->phoneNumber,
        'phone2' =>  $faker->phoneNumber,
        'facebook' =>"https://www.facebook.com/",
        'twitter' => "https://www.twitter.com/",
        'linkedin' =>  "https://www.linkedin.com/",
        'activite_actuelle' => $faker->word,
        'derniere_profession' => $faker->jobTitle,
        'secteur_activite_id' => factory(App\SecteurActivite::class),
        // 'formation_id' => $type =="admin"  ? null : factory(App\Formation::class),
        // 'pays_residence_id' => $type =="admin"  ? null : ( $pays ? $pays->id : factory(App\Pays::class)),
        'created_by' => 1,
        // 'created_by' => factory(App\User::class),

    ];
});
