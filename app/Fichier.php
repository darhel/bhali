<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fichier extends Model
{
    protected $guarded = [];


    public function projet()
    {
        return $this->belongsTo("App\Projet","projet_id");
    }

    public function entreprise()
    {
        return $this->belongsTo("App\Entreprise","entreprise_id");
    }

    public function user()
    {
        return $this->belongsTo("App\User","membre_id");
    }
}
