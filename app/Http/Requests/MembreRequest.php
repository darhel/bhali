<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class MembreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'nom' =>"required",
            'prenom' =>"required",
            // 'description' => "required|max:150",
            'avatar' => "nullable",
            'statut_marital' => "required|in:Célibataire,Marié(e),Veuf(ve),Concubinage",
            'civilite' => "required|in:Dr,M.,Mlle,Mme,Pr",
            //  'description' => "required|max:150",
             'nationalites' => 'required|array|min:1',
             'nationalites.*' => 'distinct|exists:pays,id',
            'date_naissance' => ["required","date",function($attribute,$value,$fails)
            {
                   $now = Carbon::now()->subYears(18);
                   $data = Carbon::parse($this->input('date_naissance'));
                   if($now->isBefore($data))
                   {
                       $fails('Le membre doit avoir au moins 18 ans');
                   }
            }],

            //step 1
            'competences' => $this->input('step')=="1" ? 'required|array|min:1' : "",
            'competences.*' =>$this->input('step')=="1" ?  'distinct|exists:competences,id' :"",
            'formation_id'=>$this->input('step')=="1" ? 'required|exists:formations,id' : "",
            'secteur_activite_id'=>$this->input('step')=="1" ? 'required|exists:secteur_activites,id' : "",
            'derniere_profession'=>$this->input('step')=="1" ? 'required' : "",
            'activite_actuelle'=> $this->input('step')=="1" ? 'required' : "",

                            //step 2

            'email'=>$this->input('step')=="2" ? 'required|email' : "",
            'phone1'=>$this->input('step')=="2" ? 'required|regex:/^[0-9\-\(\)\/\+\s]*$/' : "",
            'phone2'=>$this->input('step')=="2" ? 'nullable|regex:/^[0-9\-\(\)\/\+\s]*$/' : "",
            'password'=>$this->input('step')=="2" ? 'required|string|min:8|confirmed' : "",

            'facebook'=>$this->input('step')=="2" ? ['nullable','regex:/^(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/i'] : "",
            'twitter'=>$this->input('step')=="2" ? ['nullable',"regex:/^((?:http:\/\/)?|(?:https:\/\/)?)?(?:www\.)?twitter\.com\/(\w+)$/i"] : "",
            'linkedin'=>$this->input('step')=="2" ? ['nullable'] : "",
        ];
    }

    public function messages()
    {
        return [

            //step 1
            'nom.required'=>"Le nom est requis",
            'password.required'=>"Le mot de passe est requis",
            'civilite.required'=>"La civilité est requise",
            'civilite.in'=>"La civilité est invalide",
            'statut_marital.in'=>"Le statut marital est invalide",
            'statut_marital.required'=>"Le statut marital est requis",
            'prenom.required'=>"Le prénom est requis",
            'date_naissance.required'=>"La date de naissance est requise",
            'date_naissance.date'=>"La date de naissance doit avoir un format valide",
            // 'description.required'=>"La description est requise",
            // 'promotion.required'=>"La description est requise",
            'nationalite.required'=>"La nationalité est requise est requise",
            // 'description.max'=>"La description doit avoir moins de 150 caractères",




            'activite_actuelle.required'=>"L'activité est requise",
            'derniere_profession.required'=>"La profession est requise",

            // 'pays_residence_id.required' =>"Le pays de résidence est requis",
            'formation_id.required' =>"La formation est requise",
            'secteur_activite_id.required' =>"Le secteur d'activité est requis",

            'pays_id.exists' =>"Ce pays est inconnu",

            'email.email'=>"Le format de cet e-mail est invalide",
            'phone1.regex'=>"Le n° de téléphone a un format invalide",
            'phone1.require'=>"Le n° de téléphone est requis",
            'siteweb.regex'=>"L' URL du siteweb est invalide",
            'facebook.regex'=>"L'URL de cette page facebook est invalide",


        ];
    }
}
