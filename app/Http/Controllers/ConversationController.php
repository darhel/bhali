<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\Facades\Auth;
use App\Http\repository\messageRepository;

class ConversationController extends Controller
{

    public function __construct(messageRepository $conversationRepository, AuthManager $auth ){
        $this->r = $conversationRepository;
        $this->auth = $auth;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->r->getConversation($this->auth->user()->id);
        // return User::select('name','id')->where("id","!=",Auth()->user()->id)->orderBy("created_at",'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $messages = Message::create([
                'content' => $request->content,
                'to_id' => $request->to_id,
                'from_id' => Auth::user()->id,
            ]);
            DB::commit();
            return response()->json(['success' => true,'messages'=> $messages->load(['user'])],200);
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $users = User::select('name','id')->where("id","!=",Auth()->user()->id)->orderBy("created_at",'desc')->get();
        Message::where('From_id',Auth()->user()->id)->where('to_id',$id)->get();
        // return ['success'=>true,'users'=>$users,'user'=>$user];
        // return User::select('name','id')->with(['messages'])->withCount('messages')->where("id","!=",Auth()->user()->id)->orderBy("created_at",'desc')->get();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
