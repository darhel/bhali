<?php

namespace App\Http\Controllers;

use App\Role;
use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      // $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');

        return  Role::withCount(['permissions'])->search($q)->orderBy("created_at",'desc')->paginate($per);

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'libelle' => 'required|unique:roles|max:255',
        ]);

        $role = Role::create([
            'libelle' => $request->input('libelle'),
            'nom' => $request->input('libelle'),
        ]);

        $role->permissions()->attach($request->input('permissions'));

        return response()->json([
            'message' => 'Role ajouté avec succès'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        dd('okkkkk');
        return  [ "role" => $role->load('permissions'), "permissions"=>Permission::all()] ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {


        $validatedData = $request->validate([
            'libelle' => 'required|max:255|unique:roles,libelle,'.$role->id
        ]);

        $role->libelle = $request->input('libelle');

        $role->save();

        $role->permissions()->sync($request->input('permissions'));

        return response()->json([
            'message' => 'Role modifié avec succès',
            'entity' => $role],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {

        //on supprime
        $role->delete();
        return response()->json([
            'message' => 'Role supprimé avec succès',
            'entity' => $role],200);

    }
}
