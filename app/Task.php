<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];

    public function creatable()
    {
        return $this->morphTo();
    }

    public function projet()
    {
        return $this->belongsTo("App\Projet", "projet_id");
    }

    public function user()
    {
        return $this->morphTo("App\User", "responsable_id");
    }
}
