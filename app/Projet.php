<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Projet extends Model
{
    protected $guarded=[];
    protected $appends =["sumDescription"];



    public function getsumDescriptionAttribute()
    {
        return substr($this->description,0,300)."...";
    }

            /**
     * Get the owning commentable model.
     */
    public function creatable()
    {
        return $this->morphTo();
    }

    public function entreprise()
    {
        return $this->belongsTo("App\Entreprise", "entreprise_id");
    }

    public function user()
    {
        return $this->morphTo("App\User", "responsable_id");

    }

    public function secteur()
    {
        return $this->belongsTo("App\SecteurActivite", "secteur_activite_id");
    }

    public function pays()
    {
        return $this->belongsTo("App\Pays","pays_id");

    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'projet_id');
    }
    public function tasks()
    {
        return $this->hasMany('App\Task', 'projet_id');
    }

    static function avancement($projet_id){
        return DB::table('tasks')->where('projet_id',$projet_id)->avg('avancement');
    }
    //Fichier
    public function fichiers()
    {
        return $this->hasMany('App\Fichier', 'projet_id');
    }
}
